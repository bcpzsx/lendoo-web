### 与上个版本相比

更小更轻，部署更方便

### 技术栈

Node + Express + express-ejs-layouts + Element-UI + LeanCloud

### 安装

- 下载源码
- 运行命令lean switch，将自动与你的项目app id与app secret关联，命令行使用说明详见[https://leancloud.cn/docs/leanengine_cli.html](https://leancloud.cn/docs/leanengine_cli.html)
- lean deploy

说明：由于这个项目用到云函数做微信支付，所以还需要配置变量，Lean Cloud后台设置小程序的全局变量，详见[https://leancloud.cn/docs/weapp.html#配置](https://leancloud.cn/docs/weapp.html#配置)

### 数据包

https://git.oschina.net/dotton/lendoo-wx项目根目录的db.tar.gz中

### 管理员添加

在LeanCloud后台，找到你的应用中的_User表，添加username与password，那它就是后台管理员了

### 演示地址

[http://laeser.leanapp.cn/manager/login](http://laeser.leanapp.cn/manager/login)

### 截图

![分类.png](http://upload-images.jianshu.io/upload_images/2599324-4e498406a2cb28ac.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![商品添加.jpg](http://upload-images.jianshu.io/upload_images/2599324-e5583b29a93ce977.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![商品列表.jpg](http://upload-images.jianshu.io/upload_images/2599324-f59c555d8251cebe.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![订单列表.jpg](http://upload-images.jianshu.io/upload_images/2599324-d8d989fdd3a7b7ea.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 商用版已经发布

有需要的话可以加QQ: 185040150，体验扫下方小程序码，相应web后台是：[https://mall.it577.net/](https://mall.it577.net/)，密码与用户名相同

![识别体验](http://upload-images.jianshu.io/upload_images/2599324-0e638ee1e87e2ea6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 下载地址

公众号【黄秀杰】中，回复113
